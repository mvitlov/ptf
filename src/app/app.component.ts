import { Component, ViewChild } from '@angular/core';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { Config, Nav, Platform, App } from 'ionic-angular';
import 'chart.js';
import { Settings } from '../providers';
import { AngularFireAuth } from 'angularfire2/auth';
import { ProfileProvider } from '../providers/profile/profile';
import { PlayerProfile } from '../models/index';
import { AuthenticationProvider } from '../providers/authentication/authentication';
import { Profile, Signin, CreatePlayer } from '../pages/';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';

@Component({
  templateUrl: './app.component.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  rootPage = 'SigninPage';
  activePage: any;
  loader = this.loading.create({
    enableBackdropDismiss: false,
    dismissOnPageChange: false,
    showBackdrop: true
  });

  public playerProfile: PlayerProfile;

  pages: any[] = [
    { title: 'POČETNA', type: 'header' },
    { title: 'Moja Statistika', type: 'page', component: 'ProfilePage', icon: 'person' },
    { title: 'PREGLED', type: 'header' },
    { title: 'Moje Utakmice', type: 'page', component: 'GamesListPage', icon: 'trophy' },
    { title: 'Lista Igrača', type: 'page', component: 'PlayersListPage', icon: 'people' },
    { title: 'ODIGRAJ', type: 'header' },
    { title: 'Nova Utakmica', type: 'page', component: 'GameNewPage', icon: 'play' },
    { title: 'KORISNIČKI RAČUN', type: 'header' },
    { title: 'Uredi podatke', type: 'page', component: 'ProfileEditPage', icon: 'create' },
    { title: 'Odjava', type: 'command', command: 'signout', icon: 'log-out' }

    // { title: 'Tutorial', type: 'page', component: 'TutorialPage', icon: 'person' },
    // { title: 'Welcome', type: 'page', component: 'WelcomePage', icon: 'person' },
    // { title: 'Settings', type: 'page', component: 'SettingsPage', icon: 'person' },
  ];

  constructor(
    private platform: Platform,
    private settings: Settings,
    private config: Config,
    private statusBar: StatusBar,
    private splashScreen: SplashScreen,
    private afAuth: AngularFireAuth,
    private profile: ProfileProvider,
    private auth: AuthenticationProvider,
    private app: App,
    private loading: LoadingController
  ) {
    this.loader.present();
    this.afAuth.authState.subscribe(user => {
      if (!user) {
        this.rootPage = Signin;
        this.loader.dismiss();
      } else {
        this.profile.profileExists(user.uid).then(profile => {
          if (profile) {
            this.playerProfile = profile;
            this.rootPage = Profile;
            this.loader.dismiss();
          } else {
            this.rootPage = CreatePlayer;
            this.loader.dismiss();
          }
          this.activePage = this.pages.find(page => page['component'] === this.rootPage);
        });
      }
    });

    this.initApp();
  }

  initApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  processCommand(command: string) {
    switch (command) {
      case 'signout': {
        this.auth
          .signOutUser()
          .then(() => this.nav.setRoot('SigninPage'))
          .catch(error => console.error(error));
      }
    }
  }

  checkActivePage(page) {
    return page === this.activePage;
  }

  openPage(page) {
    this.nav.setRoot(page.component);
    this.activePage = page;
  }
}
