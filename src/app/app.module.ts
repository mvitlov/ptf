import { HttpClient, HttpClientModule } from '@angular/common/http';
import { registerLocaleData } from '@angular/common';
import localeHr from '@angular/common/locales/hr';

import { /* ErrorHandler,  */ NgModule, LOCALE_ID } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Camera } from '@ionic-native/camera';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { NativeAudio } from '@ionic-native/native-audio/ngx';
import { AppUpdate } from '@ionic-native/app-update';
import { IonicStorageModule, Storage } from '@ionic/storage';
import { IonicApp, /* IonicErrorHandler,  */ IonicModule } from 'ionic-angular';

import { ChartsModule } from 'ng2-charts';

import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireStorageModule } from 'angularfire2/storage';
import { firebaseConfig } from './config';

import { Settings } from '../providers';
import { MyApp } from './app.component';
import { AuthenticationProvider } from '../providers/authentication/authentication';
import { ProfileProvider } from '../providers/profile/profile';
import { GameProvider } from '../providers/game/game';
import { GameplayProvider } from '../providers/gameplay/gameplay';
import { AudioProvider } from '../providers/audio/audio';

registerLocaleData(localeHr);

export function provideSettings(storage: Storage) {
  /**
   * The Settings provider takes a set of default settings for your app.
   *
   * You can add new settings options at any time. Once the settings are saved,
   * these values will not overwrite the saved values (this can be done manually if desired).
   */
  return new Settings(storage, {
    option1: true,
    option2: 'Ionitron J. Framework',
    option3: '3',
    option4: 'Hello'
  });
}

@NgModule({
  declarations: [MyApp],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),

    ChartsModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireAuthModule,
    AngularFirestoreModule.enablePersistence(),
    AngularFireStorageModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [MyApp],
  providers: [
    Camera,
    SplashScreen,
    StatusBar,
    NativeAudio,
    AppUpdate,
    { provide: Settings, useFactory: provideSettings, deps: [Storage] },
    // Keep this to enable Ionic's runtime error handling during development
    // { provide: ErrorHandler, useClass: IonicErrorHandler },
    { provide: LOCALE_ID, useValue: 'hr' }, // CROATIAN LOCALE

    AuthenticationProvider,
    ProfileProvider,
    GameProvider,
    GameplayProvider,
    AudioProvider
  ]
})
export class AppModule {}
