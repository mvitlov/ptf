import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { first } from 'rxjs/operators';
import { UserCredential } from '@firebase/auth-types';

@Injectable()
export class AuthenticationProvider {
  public userId: string;
  constructor(private afAuth: AngularFireAuth) {}

  public getUserId() {
    return this.afAuth.authState.map(user => {
      if (user) {
        this.userId = user.uid;
        return this.userId;
      }
    });
  }

  // SIGN IN USER
  public async signInUser(email: string, password: string): Promise<UserCredential> {
    return await this.afAuth.auth.signInWithEmailAndPassword(email, password);
  }

  // SIGN OUT USER
  public async signOutUser(): Promise<void> {
    return await this.afAuth.auth.signOut();
  }

  // SIGN UP USER
  public async signupUser(data): Promise<any> {
    try {
      const userCredential = await this.afAuth.auth.createUserWithEmailAndPassword(data.email, data.password);
      return userCredential.user;
    } catch (error) {
      console.error(error);
      throw new Error(error);
    }
  }

  // RETURN CURRENT USER
  public async getCurrentUser(): Promise<any> {
    return await this.afAuth.authState.pipe(first()).toPromise();
  }

  // RESET USER PASSWORD
  public async resetPassword(email: string): Promise<void> {
    return await this.afAuth.auth.sendPasswordResetEmail(email);
  }
}
