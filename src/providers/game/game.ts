import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument } from 'angularfire2/firestore';
import { map } from 'rxjs/operator/map';
import { Game } from '../../models';
import { Observable } from 'rxjs';

/*
  Generated class for the GamesProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class GameProvider {
  constructor(private firestore: AngularFirestore) {}

  public getPlayerGames(id: string): Observable<Game[]> {
    // return new Observable(obs => {
    //   const aG = JSON.parse(allGames);
    //   obs.next(aG);
    // });
    return this.firestore
      .collection('game', qry => qry.where('playerIDs', 'array-contains', id).where('finished', '==', true))
      .valueChanges()
      .map(games => {
        const playerGames: Game[] = [];
        games.forEach(game => playerGames.push(new Game(game)));
        return playerGames;
      });
  }

  public getGameById(gameId: string): Observable<Game> {
    return this.firestore
      .doc(`/game/${gameId}`)
      .valueChanges()
      .map(game => new Game(game));
  }

  public async createNewGame(): Promise<string> {
    const gameId = await this.firestore.createId();
    const gameDocRef = this.firestore.doc(`/game/${gameId}`);

    await gameDocRef.set({ id: gameId, finished: false });
    return gameId;
  }

  public async deleteGame(gameId: string): Promise<void> {
    const gameDocRef = this.firestore.doc(`/game/${gameId}`);
    return await gameDocRef.delete();
  }

  public async saveGame(gameId: string, data: any) {
    const gameDocRef: AngularFirestoreDocument<any> = this.firestore.doc(`/game/${gameId}`);

    return await gameDocRef.update({
      playerIDs: data.playerIDs,
      players: data.players,
      points: data.points,
      finished: data.finished,
      timeFinished: data.timeFinished
    });
  }
}
