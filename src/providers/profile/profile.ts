import { Injectable } from '@angular/core';
import { AngularFireStorage } from 'angularfire2/storage';
import { AngularFirestore, AngularFirestoreDocument, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators/first';
import * as firebase from 'firebase/app';

import { Game, PlayerProfile, Team, Player, PlayerStats } from '../../models/';
import { AuthenticationProvider } from '../authentication/authentication';
import { filter, map } from 'rxjs/operators';
import { FirebaseApp } from '@firebase/app-types';
import { AngularFireAuth } from 'angularfire2/auth';
import { User, AuthCredential } from '@firebase/auth-types';

@Injectable()
export class ProfileProvider {
  public playerGames: Game[];
  public user: User;
  public playerProfile: AngularFirestoreDocument<PlayerProfile>;

  constructor(private storage: AngularFireStorage, private firestore: AngularFirestore, private auth: AuthenticationProvider, private afauth: AngularFireAuth) {
    this.afauth.auth.onAuthStateChanged(user => {
      if (user) {
        this.user = user;
        this.playerProfile = this.firestore.doc(`/playerProfile/${user.uid}`);
      }
    });
  }

  /* CHECK IF PLAYER PROFILE FOR CURRENT USER */
  public async profileExists(id: string): Promise<any> {
    return await this.firestore
      .doc(`/playerProfile/${id}`)
      .valueChanges()
      .pipe(first())
      .toPromise();
  }

  /* GET PLAYER PROFILE BY ID */
  public getPlayerProfile(id: string): Observable<PlayerProfile> {
    const playerDocRef: AngularFirestoreDocument<PlayerProfile> = this.firestore.doc(`/playerProfile/${id}`);
    return playerDocRef.valueChanges();
  }

  public getAllPlayers(id: string): Observable<PlayerProfile[]> {
    const playersRef: AngularFirestoreCollection<PlayerProfile> = this.firestore.collection('/playerProfile');
    return playersRef.valueChanges().pipe(map(players => players.filter(player => player.id !== id)));
  }

  /* CREATE NEW PLAYER PROFILE */
  public async createPlayerProfile(newPlayerForm: PlayerProfile, playerImage: File | string): Promise<void> {
    try {
      const user = await this.auth.getCurrentUser();
      const imageUrl: string = playerImage instanceof File ? await this.uploadImage(playerImage, user.uid) : playerImage;
      const playerProfileDoc: AngularFirestoreDocument<PlayerProfile> = this.firestore.doc(`playerProfile/${user.uid}`);

      await playerProfileDoc.set({
        id: user.uid,
        email: user.email,
        fullName: newPlayerForm.fullName,
        handeness: newPlayerForm.handeness,
        position: newPlayerForm.position,
        status: newPlayerForm.status,
        profileImage: imageUrl
      });
    } catch (error) {
      console.log('create player profile', error);
      throw new Error(error);
    }
  }

  public async updateName(name: string) {
    if (name) {
      return await this.playerProfile.update({ fullName: name });
    }
  }

  public async updateStatus(status: string) {
    if (status) {
      return await this.playerProfile.update({ status: status });
    }
  }

  public async updateHandeness(handeness: string) {
    if (handeness) {
      return await this.playerProfile.update({ handeness: handeness });
    }
  }

  public async updatePosition(position: string) {
    if (position) {
      return await this.playerProfile.update({ position: position });
    }
  }

  public async updateEmail(newEmail: string, password: string) {
    const credential: AuthCredential = firebase.auth.EmailAuthProvider.credential(this.user.email, password);
    return this.user
      .reauthenticateWithCredential(credential)
      .then(user => {
        this.user
          .updateEmail(newEmail)
          .then(user => {
            this.playerProfile.update({ email: newEmail });
          })
          .catch(error => {
            throw new Error(error);
          });
      })
      .catch(error => {
        throw new Error(error);
      });
  }

  public async updatePassword(newPassword: string, oldPassword: string) {
    const credential: AuthCredential = firebase.auth.EmailAuthProvider.credential(this.user.email, oldPassword);
    return this.user
      .reauthenticateWithCredential(credential)
      .then(user => {
        this.user
          .updatePassword(newPassword)
          .then(user => {
            console.log('Password Changed');
          })
          .catch(error => {
            throw new Error(error);
          });
      })
      .catch(error => {
        throw new Error(error);
      });
  }

  /* UPLOAD IMAGE TO FIREBASE STORAGE AND RETURN URL REFERENCE */
  private async uploadImage(playerImage: File, userId: string): Promise<string> {
    try {
      // const metadata: firebase.storage.UploadMetadata = { contentType: playerImage.type };
      const metadata: any = { contentType: playerImage.type };
      const imageRef = this.storage.ref(`/playerImages/${userId}`);
      const task = await imageRef.put(playerImage, metadata);

      return await imageRef.getDownloadURL().toPromise();
    } catch (error) {
      console.log('upload image', error);
      throw new Error(error);
    }
  }

  public getStats(games: Game[], userId: string): PlayerStats {
    if (!games || !games.length) {
      return;
    }

    this.playerGames = games;

    const gamesPlayed: number = games.length;
    const goalsFor: number = this.getGoalsFor(games, userId);
    const gamesWon: number = this.getGamesWon(games, userId);
    const playedGames: any[] = this.getPlayedGames(games, userId);
    const goalsPercentage: number = goalsFor / gamesPlayed;
    const winningPercentage: number = gamesWon / gamesPlayed;

    return {
      gamesPlayed: gamesPlayed,
      goalsFor: goalsFor,
      gamesWon: gamesWon,
      playedGames: playedGames,
      goalsPercentage: goalsPercentage,
      winningPercentage: winningPercentage
    };
  }

  private getGoalsFor(games: Game[], userId: string): number {
    let goalsFor = 0;

    games.forEach(game => {
      game.points.forEach(point => {
        if (!point.against && point.playerId === userId) {
          goalsFor++;
        }
      });
    });

    return goalsFor;
  }

  private getGamesWon(games: Game[], userId: string): number {
    let gamesWon = 0;

    games.forEach(game => {
      game.teams.forEach(team => {
        if (team.winners && team.players.some(player => player.playerId === userId)) {
          gamesWon++;
        }
      });
    });

    return gamesWon;
  }

  public getPlayedGames(games: Game[], userId: string): any[] {
    if (!games || !games.length) {
      return [];
    }

    const list = [];
    games.forEach((game: Game) => {
      const result = {
        gameId: game.id,
        winners: {},
        loosers: {},
        gameImage: this.getGameImage(game.teams, userId)
      };

      const score = { winners: 0, loosers: 0 };

      game.teams.forEach(team => {
        if (team.winners) {
          result.winners = {
            side: team.side,
            players: team.players.map(player => this.getPlayerProfile(player.playerId).map(player => player.fullName))
          };
          score.winners += team.scoreFor;
          score.loosers += team.scoreAgainst;
        } else {
          result.loosers = {
            side: team.side,
            players: team.players.map(player => this.getPlayerProfile(player.playerId).map(player => player.fullName))
          };
          score.loosers += team.scoreFor;
          score.winners += team.scoreAgainst;
        }
        result.winners['score'] = score.winners;
        result.loosers['score'] = score.loosers;
      });

      list.push(result);
    });

    return list;
  }

  private getGameImage(teams: Team[], id: string): string {
    const baseUrl = 'https://ui-avatars.com/api/?color=fff&size=128&name=';
    const bgBlue = '3498db';
    const bgRed = 'e74c3c';
    let retUrl: string;

    teams.forEach(team => {
      if (team.players.some(player => player.playerId === id)) {
        if (team.winners) {
          retUrl = `${baseUrl}W&background=${team.side === 'blue' ? bgBlue : bgRed}`;
        } else {
          retUrl = `${baseUrl}L&background=${team.side === 'blue' ? bgBlue : bgRed}`;
        }
      }
    });
    return retUrl;
  }
}
