import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Platform } from 'ionic-angular';
import { NativeAudio } from '@ionic-native/native-audio/ngx';

interface Sound {
  key: string;
  asset: string;
  isNative: boolean;
}

const crowdSound = '/assets/audio/crowd.mp3';
const goalSound = '/assets/audio/goal.mp3';
const whistleSound = '/assets/audio/whistle.mp3';

@Injectable()
export class AudioProvider {
  private sounds: Sound[] = [];
  private audioPlayer: HTMLAudioElement = new Audio();
  private forceWebAudio: boolean = true;

  constructor(private platform: Platform, private nativeAudio: NativeAudio) {
    this.preloadComplex('crowd', crowdSound);
    this.preload('goal', goalSound);
    this.preload('whistle', whistleSound);
  }

  public stopAllSounds() {
    this.sounds.forEach(sound => {
      if (sound.isNative) {
        this.nativeAudio.stop(sound.key);
      } else {
        this.audioPlayer.pause();
      }
    });
  }

  preload(key: string, asset: string): void {
    if (this.platform.is('cordova') && !this.forceWebAudio) {
      this.nativeAudio.preloadSimple(key, asset);

      this.sounds.push({
        key: key,
        asset: asset,
        isNative: true
      });
    } else {
      let audio = new Audio();
      audio.src = asset;

      this.sounds.push({
        key: key,
        asset: asset,
        isNative: false
      });
    }
  }

  preloadComplex(key: string, asset: string): void {
    if (this.platform.is('cordova') && !this.forceWebAudio) {
      this.nativeAudio.preloadComplex(key, asset, 0.8, 1, 0);

      this.sounds.push({
        key: key,
        asset: asset,
        isNative: true
      });
    } else {
      let audio = new Audio();
      audio.src = asset;
      audio.loop = true;

      this.sounds.push({
        key: key,
        asset: asset,
        isNative: false
      });
    }
  }

  loop(key: string): void {
    let soundToPlay = this.sounds.find(sound => {
      return sound.key === key;
    });

    if (soundToPlay.isNative) {
      this.nativeAudio.loop(soundToPlay.asset).then(
        res => {
          console.log(res);
        },
        err => {
          console.log(err);
        }
      );
    } else {
      this.audioPlayer.src = soundToPlay.asset;
      this.audioPlayer.play();
    }
  }

  play(key: string): Promise<void> {
    let soundToPlay = this.sounds.find(sound => sound.key === key);

    if (soundToPlay.isNative) {
      return this.nativeAudio.play(soundToPlay.asset).then(
        res => {
          console.log(res);
        },
        err => {
          console.log(err);
        }
      );
    } else {
      this.audioPlayer.src = soundToPlay.asset;
      return this.audioPlayer.play();
    }
  }
}
