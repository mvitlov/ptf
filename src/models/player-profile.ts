export interface PlayerProfile {
  id: string;
  email: string;
  fullName: string;
  handeness: string;
  position: string;
  profileImage: string;
  status: string;
}
