import { Team } from './team';
import { Player } from './player';
import { Side } from './side';
import { Point } from './point';

interface FirestoreDate {
  seconds: number;
  nanoseconds: number;
}

export class Game {
  id: string;
  finished: boolean;
  finishedTime: FirestoreDate;
  points: Point[] = [];
  players: Player[] = [];
  teams: Team[] = [];

  constructor(data: any) {
    this.id = data.id;
    this.finished = data.finished;
    this.finishedTime = data.timeFinished;
    this.players = data.players;
    this.points = data.points;
    this.setTeams();
  }

  private setTeams() {
    const redTeam = new Team(Side.red);
    const blueTeam = new Team(Side.blue);
    const score = { red: 0, blue: 0 };

    this.players.forEach(player => {
      player.side === Side.red
        ? redTeam.players.push({
            playerId: player.playerId,
            playerPosition: player.playerPosition
          })
        : blueTeam.players.push({
            playerId: player.playerId,
            playerPosition: player.playerPosition
          });
    });

    this.points.forEach(point => {
      if (redTeam.players.map(player => player.playerId).indexOf(point.playerId) > -1) {
        if (point.against) {
          score.blue++;
          redTeam.scoreAgainst++;
        } else {
          score.red++;
          redTeam.scoreFor++;
        }
      } else {
        if (point.against) {
          score.red++;
          blueTeam.scoreAgainst++;
        } else {
          score.blue++;
          blueTeam.scoreFor++;
        }
      }
    });

    blueTeam.winners = score.blue > score.red;
    redTeam.winners = score.red > score.blue;

    this.teams = [redTeam, blueTeam];
  }
}
