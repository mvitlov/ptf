export interface Point {
  against: boolean;
  playerId: string;
  time: number;
}
