import { Side } from './side';

export class GameTimeline {
  name: string;
  timeOffset: number;
  time: string;
  text: string;
  score: string;
  side: string;

  constructor(name: string, timeOffset: number, text: string, score: string, side: Side) {
    this.name = name;
    this.timeOffset = timeOffset;
    this.time = this.formatTime(timeOffset);
    this.text = text;
    this.score = score;
    this.side = side === Side.blue ? 'blue' : 'red';
  }

  private formatTime(seconds: number): string {
    return `${Math.floor(seconds / 60)}' ${this.zeroFill(seconds % 60)}"`;
  }

  private zeroFill(value: number): string {
    return value > 9 ? String(value) : `0${value}`;
  }
}
