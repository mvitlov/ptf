export enum GameState {
  NotStarted,
  Playing,
  Paused,
  Finished
}
