import { PlayerProfile } from './player-profile';

export interface GamePlayers {
  blue1?: PlayerProfile;
  blue2?: PlayerProfile;
  red1?: PlayerProfile;
  red2?: PlayerProfile;
}
