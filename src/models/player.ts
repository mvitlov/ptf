import { TeamPlayer } from './team-player';
import { Side } from './side';

export interface Player extends TeamPlayer {
  side: Side;
}
