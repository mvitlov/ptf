export interface PlayerStats {
  gamesPlayed: number;
  goalsFor: number;
  gamesWon: number;
  playedGames: any[];
  goalsPercentage: number;
  winningPercentage: number;
}
