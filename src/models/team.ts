import { Side } from './side';
import { TeamPlayer } from './team-player';

export class Team {
  side: Side;
  players: TeamPlayer[] = [];
  scoreFor = 0;
  scoreAgainst = 0;
  winners: boolean;
  constructor(side: Side) {
    this.side = side;
  }
}
