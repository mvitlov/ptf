import { PlayerPosition } from './position';

export interface TeamPlayer {
  playerId: string;
  playerPosition: PlayerPosition;
}
