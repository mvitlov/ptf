import { PlayerProfile } from './player-profile';

export class GamePoint {
  time: number;
  against: boolean;
  player: PlayerProfile;

  constructor(time: number, against: boolean, player: PlayerProfile) {
    this.time = time;
    this.against = against;
    this.player = player;
  }
}
