import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ProfileProvider } from '../../providers/profile/profile';
import { AuthenticationProvider } from '../../providers/authentication/authentication';
import { Observable } from 'rxjs/Observable';
import { PlayerProfile } from '../../models';

/**
 * Generated class for the PlayersListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-players-list',
  templateUrl: 'players-list.html'
})
export class PlayersListPage {
  public playersList: Observable<PlayerProfile[]>;

  constructor(public navCtrl: NavController, public navParams: NavParams, private auth: AuthenticationProvider, private profile: ProfileProvider) {
    this.auth.getUserId().subscribe(userId => {
      this.playersList = this.profile.getAllPlayers(userId);
    });
  }

  public openPlayerStats(player: PlayerProfile): void {
    this.navCtrl.push('PlayerProfilePage', { player: player });
  }
}
