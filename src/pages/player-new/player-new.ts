import { Component, OnInit, AfterViewInit, OnDestroy, ElementRef, ViewChild } from '@angular/core';
import { IonicPage, NavController, LoadingController, MenuController } from 'ionic-angular';
import { SafeUrl, DomSanitizer } from '@angular/platform-browser';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ProfileProvider } from '../../providers/profile/profile';
import { colors } from '../../other/colors';

/**
 * Generated class for the PlayerNewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-player-new',
  templateUrl: 'player-new.html'
})
export class PlayerNewPage implements OnInit, AfterViewInit {
  @ViewChild('fileInput') inputElement: ElementRef;
  public imageUrl: SafeUrl;
  defaultImage: string;
  newPlayerForm: FormGroup;
  constructor(
    private navCtrl: NavController,
    private formBuilder: FormBuilder,
    private sanitizer: DomSanitizer,
    private loading: LoadingController,
    private menu: MenuController,
    private profile: ProfileProvider
  ) {
    this.newPlayerForm = this.formBuilder.group({
      fullName: ['', Validators.compose([Validators.required, Validators.pattern(`^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$`)])],
      profileImage: [''],
      status: [''],
      handeness: ['Desna'],
      position: ['Napad']
    });
  }

  ionViewDidEnter() {
    this.menu.enable(false);
  }

  ngOnInit() {
    this.setImage('?');
  }

  ngAfterViewInit() {
    if (this.inputElement) {
      const inputElement: HTMLInputElement = this.inputElement.nativeElement;
      inputElement.addEventListener('change', () => this.onImageChange(inputElement));
    }
  }

  private setImage(tag: string): void {
    this.defaultImage = `https://ui-avatars.com/api/?name=${tag}&background=${this.getRandomColor()}&color=fff&size=256`;
  }

  private getRandomColor(): string {
    return colors[Math.floor(Math.random() * colors.length)];
  }

  public clearImage(): void {
    this.imageUrl = '';
  }

  public changeImage(tag): void {
    if (!this.imageUrl) {
      const str = tag.value;
      this.setImage(str.split(' ').join('+'));
    }
  }

  private onImageChange(inputElement: HTMLInputElement): void {
    if (inputElement.files && inputElement.files[0]) {
      const reader = new FileReader();
      reader.onload = e => (this.imageUrl = this.sanitizer.bypassSecurityTrustUrl((<any>e.target).result));
      reader.readAsDataURL(inputElement.files[0]);
    }
  }

  public async createNewPlayer(newPlayerForm: FormGroup): Promise<void> {
    const loading = await this.loading.create();
    loading.present();
    let playerImage: File | string;
    const inputEl: HTMLInputElement = this.inputElement.nativeElement;
    if (inputEl.files.length > 0) {
      playerImage = inputEl.files.item(0);
    } else {
      playerImage = this.defaultImage;
    }
    try {
      await this.profile.createPlayerProfile(newPlayerForm.value, playerImage);
      await loading.dismiss();
      this.navCtrl.setRoot('ProfilePage', '', { animate: true, direction: 'forward' });
    } catch (error) {
      console.log(error);
    }
  }

  ionViewWillLeave() {
    this.menu.enable(true);
  }
}
