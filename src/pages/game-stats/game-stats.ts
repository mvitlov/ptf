import { Component, OnInit } from '@angular/core';
import { IonicPage, NavParams } from 'ionic-angular';
import { Game, GameTimeline, Side, TeamPlayer, Player, Point } from '../../models/';
import { ProfileProvider } from '../../providers/profile/profile';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operator/map';
import { take } from 'rxjs/operators/take';
import { GameProvider } from '../../providers/game/game';

/**
 * Generated class for the GameStatsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-game-stats',
  templateUrl: 'game-stats.html'
})
export class GameStatsPage {
  public gameData: Game;
  private gameId: string = this.navParams.get('gameId');

  public playerMap;
  public score: { blue: number; red: number };
  public timeline: GameTimeline[] = [];
  public chartDatasets: Array<any> = [
    {
      lineTension: 0,
      fill: false,
      data: [],
      label: 'Ekipa CRVENI'
    },
    {
      lineTension: 0,
      fill: false,
      data: [],
      label: 'Ekipa PLAVI'
    }
  ];

  constructor(public navParams: NavParams, private profile: ProfileProvider, private game: GameProvider) {
    this.game.getGameById(this.gameId).subscribe(game => {
      this.gameData = game;
      this.processGame(this.gameData);
    });
  }

  public getPlayers(side: string) {
    return this.playerMap.filter(player => player.side === side);
  }

  private async processGame(game: Game) {
    const playersMap = await this.processPlayers(game.players);

    playersMap.subscribe(result => {
      this.playerMap = result;
      this.processPoints(game.points, result);
    });
  }

  private async processPlayers(players: Player[]) {
    let obs = [];
    players.forEach(player => {
      obs.push(
        this.profile
          .getPlayerProfile(player.playerId)
          .map(profile => ({ playerId: player.playerId, side: player.side, position: player.playerPosition || '', playerName: profile.fullName }))
          .pipe(take(1))
      );
    });

    return Observable.forkJoin(obs);
  }

  private async processPoints(points: Point[], playerMap: any) {
    const dataBlue = [{ x: 0, y: 0 }];
    const dataRed = [{ x: 0, y: 0 }];

    let redScore = 0;
    let blueScore = 0;

    points.forEach(point => {
      const name = point.playerId;
      const gamePlayer = playerMap.filter(player => player.playerId === name);
      const text = gamePlayer[0].playerName + (point.against ? ' zabija... AUTOGOL!' : ' zabija GOL!');
      const side = gamePlayer[0].side;

      switch (side) {
        case Side.blue: {
          point.against ? redScore++ : blueScore++;
          break;
        }
        case Side.red: {
          point.against ? blueScore++ : redScore++;
          break;
        }
      }
      this.timeline.push(new GameTimeline(name, point.time, text, `${blueScore}-${redScore}`, gamePlayer.side));
      dataRed.push({
        x: point.time,
        y: redScore
      });
      dataBlue.push({
        x: point.time,
        y: blueScore
      });
    });

    this.score = { blue: blueScore, red: redScore };

    this.chartDatasets[0].data = dataRed;
    this.chartDatasets[1].data = dataBlue;
    this.chartDatasets = this.chartDatasets.slice(); // force refresh of chart

    this.timeline.sort((t1, t2) => t2.timeOffset - t1.timeOffset);
  }
}
