import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChartsModule } from 'ng2-charts';
import { GameStatsPage } from './game-stats';
import { StatsHeaderComponent } from './game-stats-components/stats-header/stats-header';
import { StatsChartComponent } from './game-stats-components/stats-chart/stats-chart';
import { StatsTimelineComponent } from './game-stats-components/stats-timeline/stats-timeline';

@NgModule({
  declarations: [GameStatsPage, StatsHeaderComponent, StatsChartComponent, StatsTimelineComponent],
  imports: [IonicPageModule.forChild(GameStatsPage), ChartsModule]
})
export class GameStatsPageModule {}
