import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'stats-header',
  templateUrl: './stats-header.html'
})
export class StatsHeaderComponent implements OnInit {
  @Input() score: any;

  constructor() {}

  ngOnInit() {}

  public getImage(side: string): string {
    let retUrl: string;
    const baseUrl = 'https://ui-avatars.com/api/?size=128&color=fff&';
    const bgBlue = '3498db';
    const bgRed = 'e74c3c';

    switch (side) {
      case 'blue': {
        if (this.score.blue > this.score.red) {
          retUrl = `${baseUrl}name=W&background=${bgBlue}`;
        } else {
          retUrl = `${baseUrl}name=L&background=${bgBlue}`;
        }
        break;
      }
      case 'red': {
        if (this.score.red > this.score.blue) {
          retUrl = `${baseUrl}name=W&background=${bgRed}`;
        } else {
          retUrl = `${baseUrl}name=L&background=${bgRed}`;
        }
        break;
      }
    }
    return retUrl;
  }
}
