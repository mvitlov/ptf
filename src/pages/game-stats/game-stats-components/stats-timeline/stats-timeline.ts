import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'stats-timeline',
  templateUrl: 'stats-timeline.html'
})
export class StatsTimelineComponent implements OnInit {
  @Input() timeline;
  constructor() {}

  ngOnInit() {}
}
