import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'stats-chart',
  templateUrl: './stats-chart.html'
})
export class StatsChartComponent implements OnInit {
  @Input() chartDatasets;
  @Input() gameID;
  @Input() finishedTime;

  //STATIC CHART PROPERTIES
  public lineChartOptions = {
    responsive: true,
    scales: {
      xAxes: [
        {
          scaleID: 'x-axis-0',
          type: 'linear',
          position: 'bottom',
          gridLines: {
            color: '#2c3e50'
          },
          ticks: {
            min: 0,
            suggestedMax: 60,
            stepSize: 10,
            fontColor: '#7f8c8d',
            fontFamily: "'Exo 2', sans-serif",
            callback: function(label, index, labels) {
              return label === -1 ? '' : StatsChartComponent.formatSeconds(parseInt(label, 10));
            }
          }
        }
      ],
      yAxes: [
        {
          scaleID: 'y-axis-0',
          gridLines: {
            color: '#2c3e50'
          },
          ticks: {
            min: 0,
            suggestedMax: 10,
            stepSize: 1,
            fontColor: '#7f8c8d',
            fontFamily: "'Exo 2', sans-serif"
          }
        }
      ]
    },
    tooltips: {
      callbacks: {
        title: function(tooltipItems, data) {
          return tooltipItems.length && StatsChartComponent.formatSeconds(tooltipItems[0].xLabel);
        }
      }
    }
  };
  public lineChartColors: Array<any> = [
    {
      // RED TEAM
      backgroundColor: 'rgba(0,0,0,0)',
      borderColor: '#e74c3c',

      pointBorderColor: '#e74c3c',
      pointBackgroundColor: '#fff',
      pointBorderWidth: 1.6,
      pointRadius: 2.6,

      pointHoverRadius: 5,
      pointHoverBackgroundColor: '#e74c3c',
      pointHoverBorderColor: '#e74c3c',
      pointHoverBorderWidth: 2
    },
    {
      // BLUE TEAM
      backgroundColor: 'rgba(0,0,0,0)',
      borderColor: '#3498db',

      pointBorderColor: '#3498db',
      pointBackgroundColor: '#fff',
      pointBorderWidth: 1.6,
      pointRadius: 2.6,

      pointHoverRadius: 5,
      pointHoverBackgroundColor: '#3498db',
      pointHoverBorderColor: '#3498db',
      pointHoverBorderWidth: 2
    }
  ];
  public lineChartLegend = true;
  public lineChartType = 'line';

  public static formatSeconds = (seconds: number) => {
    if (seconds <= 0) {
      return '';
    }
    const date = new Date(null);
    date.setSeconds(seconds);
    return seconds < 3600 ? date.toISOString().substr(14, 5) : date.toISOString().substr(11, 8);
  };

  constructor() {}

  ngOnInit() {}
}
