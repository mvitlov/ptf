import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController } from 'ionic-angular';
import { GamePlayers, Point, Side, PlayerProfile, GameState, GamePoint, Game } from '../../models';
// import { gameplayers } from './dummy'; //DUMMY GAME PLAYERS
import { GameProvider } from '../../providers/game/game';
import { GameplayProvider } from '../../providers/gameplay/gameplay';

// TODO:SOUNDS
// import { AudioProvider } from '../../providers/audio/audio';

/**
 * Generated class for the GamePlayPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-game-play',
  templateUrl: 'game-play.html'
})
export class GamePlayPage {
  public gamePlayers: any = this.navParams.get('gamePlayers');
  public blueScore = 0;
  public redScore = 0;
  public elapsedTime = '';
  public gameMenu = false;
  public showPause: boolean;
  public playerSelected: { team?: Side; player?: PlayerProfile };

  private goalRule: boolean = this.navParams.get('goalRule');
  private gameId: string;
  private timeStarted: Date;
  private gamePoints: GamePoint[] = [];

  private timerId: number;
  private baseTime = new Date();
  private secondsBeforePause = 0;

  private gameState: GameState = GameState.NotStarted;

  commentatorMessage = 'GOAL!';

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private menu: MenuController,
    private game: GameProvider,
    private gameplay: GameplayProvider
  ) {}

  ionViewDidEnter() {
    this.menu.enable(false);
    // START
    this.startGame();
  }

  // INIT GAME PARAMS
  private async startGame(): Promise<void> {
    this.gameState = GameState.Playing;

    this.updateElapsedTime();

    if (!this.hasGameEnded()) {
      this.startGameTimer();

      if (!this.gameId) {
        this.createNewGame()
          .then(gameId => {
            console.log('New Game Initialized: ' + gameId);
            this.gameId = gameId;
            this.timeStarted = new Date();
          })
          .catch(error => {
            console.error(error);
          });
      }
    } else {
      this.gameState = GameState.Finished;
    }
  }

  // GET GAME ID
  private createNewGame(): Promise<string> {
    return this.game.createNewGame();
  }

  // #region TIMER
  private startGameTimer(): void {
    window.clearInterval(this.timerId);
    this.timerId = window.setInterval(() => this.updateElapsedTime(), 1000);
  }
  private stopGameTimer(): void {
    window.clearInterval(this.timerId);
  }
  private updateElapsedTime(): void {
    if (this.gameState === GameState.NotStarted) {
      this.elapsedTime = '';
    } else {
      this.elapsedTime = this.formatTimeDiff(this.getElapsedSeconds(new Date()));
    }
  }
  private getElapsedSeconds(t: Date): number {
    const msSinceBase = this.baseTime ? Math.abs(t.getTime() - this.baseTime.getTime()) : 0;
    return this.secondsBeforePause + Math.floor(msSinceBase / 1000);
  }
  private formatTimeDiff(totalSeconds: number): string {
    const days = Math.floor(totalSeconds / 86400);
    totalSeconds -= days * 86400;
    const hours = Math.floor(totalSeconds / 3600) % 24;
    totalSeconds -= hours * 3600;
    const minutes = Math.floor(totalSeconds / 60) % 60;
    totalSeconds -= minutes * 60;
    const seconds = Math.floor(totalSeconds % 60);

    const hoursStr = hours < 10 ? '0' + String(hours) : String(hours);
    const minutesStr = minutes < 10 ? '0' + String(minutes) : String(minutes);
    const secondsStr = seconds < 10 ? '0' + String(seconds) : String(seconds);
    return hoursStr === '00' ? minutesStr + ':' + secondsStr : hoursStr + ':' + minutesStr + ':' + secondsStr;
  }
  // #endregion TIMER

  private hasGameEnded(): boolean {
    // TODO: implement game rules
    const pointsToWin = 10;
    const minimumDifference = this.goalRule ? 2 : 0;
    return (
      this.gameState === GameState.Finished ||
      ((this.blueScore >= pointsToWin || this.redScore >= pointsToWin) && Math.abs(this.blueScore - this.redScore) >= minimumDifference)
    );
  }

  // #region MENU ACTIONS
  public onGameTimeClicked() {
    if (this.gameState !== GameState.Paused) {
      this.pauseGame();
      this.gameMenu = true;
    } else if (!this.gameMenu) {
      this.resumeGame();
    }
  }

  public onContinueGameClicked(): void {
    this.resumeGame();
    this.gameMenu = false;
  }

  public onUndoLastGoalClicked(): void {
    const point = this.gamePoints.pop();
    if (point) {
      const side = this.getPlayerSide(point.player);

      switch (side) {
        case Side.red: {
          point.against ? this.blueScore-- : this.redScore--;
          break;
        }
        case Side.blue: {
          point.against ? this.redScore-- : this.blueScore--;
          break;
        }
      }
    }
    this.resumeGame();
    this.gameMenu = false;
  }

  public onEndGameClicked(): void {
    this.game.deleteGame(this.gameId).then(() => {
      this.menu.enable(true);
      this.navCtrl.setRoot('ProfilePage', '', { animate: true, direction: 'back' });
    });
  }

  private pauseGame(): void {
    this.showPause = true;
    if (this.hasGameEnded()) {
      return;
    }

    this.gameState = GameState.Paused;
    this.stopGameTimer();

    this.secondsBeforePause = this.getElapsedSeconds(new Date());
    this.baseTime = null;
    this.updateElapsedTime();
  }

  private resumeGame(): void {
    this.showPause = false;

    if (this.hasGameEnded()) {
      return;
    }
    this.gameState = GameState.Playing;

    this.baseTime = new Date();
    this.updateElapsedTime();
    this.startGameTimer();
  }
  // #endregion MENU ACTIONS

  public onPlayerClicked(player: PlayerProfile, team: Side) {
    if (this.hasGameEnded()) {
      return;
    }

    if (this.gameState === GameState.Paused && this.playerSelected.player === player) {
      this.playerSelected = null;
      this.resumeGame();
      return;
    }

    if (this.gameState !== GameState.Playing) {
      return;
    }
    this.pauseGame();
    this.playerSelected = { player: player, team: team };
  }

  public onGoalClick(playerSelected: any, goal: 'own' | 'normal') {
    if (this.gameState !== GameState.Paused || this.playerSelected == null) {
      return;
    }

    this.handlePointScored(playerSelected.player, playerSelected.team, goal === 'normal' ? false : true);

    this.resumeGame();
    this.playerSelected = null;
  }

  private handlePointScored(player: PlayerProfile, team: Side, against: boolean): void {
    const now = new Date();
    let time = this.getElapsedSeconds(now);

    if (this.gamePoints.length > 0 && this.gamePoints[this.gamePoints.length - 1].time >= time) {
      time = this.gamePoints[this.gamePoints.length - 1].time + 1;
    }

    this.gamePoints.push(new GamePoint(time, against, player));

    this.scoreGoal(team, against);

    if (this.hasGameEnded()) {
      console.log('game ended!');

      this.stopGameTimer();
      this.gameState = GameState.Finished;
      this.game.saveGame(this.gameId, this.buildSaveGameParams()).then(() => {
        this.menu.enable(true);
        this.navCtrl.setRoot('GameStatsPage', { gameId: this.gameId }, { animate: true, direction: 'back' });
      });
      // this.saveGame()
      //   .then(gameId => {
      //     console.log('Game created: ' + gameId);
      //     this.leaveFullScreen();
      //     this.router.navigate(['games', gameId], { replaceUrl: true });
      //   })
      //   .catch(ex => {
      //     console.warn('Could not save final game. TODO: Save data in local storage');
      //     this.onlineMode = false;
      //     this.saveGameOffline()
      //       .then(game => {
      //         this.leaveFullScreen();
      //         this.router.navigate(['games', game.id], { replaceUrl: true });
      //       })
      //       .catch(ex => {
      //         console.log(ex); // TODO retry?
      //       });
      //   });
    } else {
      console.log('goal scored');
      // this.audio.playSingle(goalSound);

      // this.playSoundAfterGoal();
      // this.saveGame()
      //   .then(gameId => {
      //     // TODO show point feedback
      //   })
      //   .catch(ex => {
      //     console.log('Could not update game. Offline mode On.');
      //     this.onlineMode = false;
      //   });
    }
  }

  private scoreGoal(scorerSide: Side, against: boolean): void {
    let pointWinner: Side;

    if (scorerSide === Side.red) {
      pointWinner = against ? Side.blue : Side.red;
    } else {
      pointWinner = against ? Side.red : Side.blue;
    }

    if (pointWinner === Side.blue) {
      this.blueScore++;
    } else {
      this.redScore++;
    }

    if (scorerSide === Side.blue) {
      // this.nameClassesGamePlayScoreBlue['game-play__blue-score--goal'] = true;
      // this.nameClassesGamePlayCommentator['game-play__commentator--blue'] = true;
      // setTimeout(() => {
      //   this.nameClassesGamePlayScoreBlue['game-play__blue-score--goal'] = false;
      //   this.nameClassesGamePlayCommentator['game-play__commentator--blue'] = false;
      // }, 2000);
    } else {
      // this.nameClassesGamePlayScoreRed['game-play__red-score--goal'] = true;
      // this.nameClassesGamePlayCommentator['game-play__commentator--red'] = true;
      // setTimeout(() => {
      //   this.nameClassesGamePlayScoreRed['game-play__red-score--goal'] = false;
      //   this.nameClassesGamePlayCommentator['game-play__commentator--red'] = false;
      // }, 2000);
    }

    if (against) {
      this.commentatorMessage = 'OWN GOAL!';
    } else {
      this.commentatorMessage = 'GOAL!';
    }

    // this.nameClassesGamePlayCommentator['game-play__commentator--longtext'] = this.commentatorMessage.length > 6;

    // this.nameClassesGamePlayCommentator['game-play__commentator--active'] = true;
    // setTimeout(() => {
    //   this.nameClassesGamePlayCommentator['game-play__commentator--active'] = false;
    // }, 2000);
  }

  private buildSaveGameParams(): { [key: string]: any } {
    // PLAYERS
    const playerIDs: string[] = [];
    const players = [this.gamePlayers.blue1, this.gamePlayers.red1, this.gamePlayers.blue2, this.gamePlayers.red2].map(player => {
      playerIDs.push(player.id);

      return { playerId: player.id, side: this.getPlayerSide(player) };
    });

    // GAME POINTS
    const points = this.gamePoints.map(p => {
      return { time: p.time, playerId: p.player.id, against: p.against };
    });

    const timeFinished = new Date();
    return { points: points, players: players, playerIDs: playerIDs, finished: true, timeFinished: timeFinished, timeStarted: this.timeStarted };
  }

  private getPlayerSide(player: PlayerProfile): Side {
    if (player.id === this.gamePlayers.blue1.id || player.id === this.gamePlayers.blue2.id) {
      return Side.blue;
    }
    if (player.id === this.gamePlayers.red1.id || player.id === this.gamePlayers.red2.id) {
      return Side.red;
    }
    return null;
  }
}
