import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertController, IonicPage, Loading, LoadingController, NavController } from 'ionic-angular';

import { AuthenticationProvider } from '../../providers/authentication/authentication';
import { MenuController } from 'ionic-angular/components/app/menu-controller';

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html'
})
export class SignupPage {
  public signupForm: FormGroup;
  private loading: Loading;

  constructor(
    private alertCtrl: AlertController,
    private authProvider: AuthenticationProvider,
    private formBuilder: FormBuilder,
    private loadingCtrl: LoadingController,
    private navCtrl: NavController,
    private menu: MenuController
  ) {
    this.signupForm = this.formBuilder.group(
      {
        email: ['', Validators.compose([Validators.required, Validators.email])],
        password: ['', Validators.compose([Validators.minLength(6), Validators.required])],
        confirmPassword: ['', Validators.required]
      },
      {
        validator: [
          this.matchingPasswords('password', 'confirmPassword')
          // this.matchingDomain('email')
        ]
      }
    );
  }

  ionViewDidEnter() {
    this.menu.enable(false);
  }

  matchingDomain(emailKey: string) {
    return (group: FormGroup): { [key: string]: any } => {
      const email: string = group.controls[emailKey].value;
      if (email.indexOf('@pis.eu.com') === -1) {
        return { invalidDomain: true };
      }
    };
  }

  private matchingPasswords(passwordKey: string, confirmPasswordKey: string): { [key: string]: any } {
    return (group: FormGroup): { [key: string]: any } => {
      const password = group.controls[passwordKey];
      const confirmPassword = group.controls[confirmPasswordKey];

      if (password.value !== confirmPassword.value) {
        return {
          mismatchedPasswords: true
        };
      }
    };
  }

  public async signupUser(signupForm: FormGroup): Promise<void> {
    this.loading = await this.loadingCtrl.create();
    try {
      this.loading.present();

      await this.authProvider.signupUser(signupForm.value);
      await this.loading.dismiss();
      this.navCtrl.setRoot('PlayerNewPage', { animate: true, animationDirection: 'forward' });
    } catch (error) {
      console.log(error);

      await this.loading.dismiss();
      const alert = await this.alertCtrl.create({
        message: error.message,
        buttons: [
          {
            text: 'OK',
            role: 'cancel'
          }
        ]
      });
      alert.present();
    }
  }

  public onUserExists() {
    this.navCtrl.setRoot('SigninPage', '', { animate: true, direction: 'back' });
  }

  ionViewWillLeave() {
    this.menu.enable(true);
  }
}
