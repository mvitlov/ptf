import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';

import { ProfilePage } from './profile';
import { InfoProfileComponent } from './profile-components/info-profile/info-profile';
import { InfoStatisticsComponent } from './profile-components/info-statistics/info-statistics';
import { InfoGamesComponent } from './profile-components/info-games/info-games';

@NgModule({
  declarations: [ProfilePage, InfoProfileComponent, InfoStatisticsComponent, InfoGamesComponent],
  imports: [IonicPageModule.forChild(ProfilePage)],
  exports: []
})
export class ProfilePageModule {}
