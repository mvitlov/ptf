import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { ProfileProvider } from '../../providers/profile/profile';
import { PlayerProfile, Game } from '../../models';
import { Observable } from 'rxjs';
import { AuthenticationProvider } from '../../providers/authentication/authentication';
import { GameProvider } from '../../providers/game/game';
import { AppUpdate } from '@ionic-native/app-update';
import { updateUrl } from '../../app/config';

/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html'
})
export class ProfilePage implements OnInit {
  public playerProfile: Observable<PlayerProfile>;
  public playerStats: any;
  public loading: boolean = true;
  constructor(
    public navCtrl: NavController,
    private auth: AuthenticationProvider,
    private profile: ProfileProvider,
    private games: GameProvider,
    private update: AppUpdate
  ) {
    this.update.checkAppUpdate(updateUrl);
  }

  ngOnInit() {
    this.getProfile();
  }

  public getProfile(): void {
    this.auth.getUserId().subscribe(userId => {
      this.playerProfile = this.profile.getPlayerProfile(userId);
      this.getPlayerGames(userId);
    });
  }

  private getPlayerGames(userId: string) {
    this.games.getPlayerGames(userId).subscribe((playerGames: Game[]) => {
      if (playerGames && playerGames.length) {
        this.playerStats = this.profile.getStats(playerGames, userId);
      } else {
        this.loading = false;
      }
    });
  }

  public onNewGame() {
    this.navCtrl.setRoot('GameNewPage', '', { animate: true, direction: 'forward' });
  }

  public onEditClick() {
    this.navCtrl.push('ProfileEditPage');
  }
}
