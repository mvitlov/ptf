import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { PlayerProfile } from '../../../../models';

@Component({
  selector: 'info-profile',
  templateUrl: './info-profile.html'
})
export class InfoProfileComponent implements OnInit {
  @Input() playerProfile: Observable<PlayerProfile>;
  @Input() loading: boolean;

  constructor() {}

  ngOnInit() {}
}
