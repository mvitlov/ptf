import { Component, Input } from '@angular/core';
import { PlayerStats } from '../../../../models';

@Component({
  selector: 'info-statistics',
  templateUrl: './info-statistics.html'
})
export class InfoStatisticsComponent {
  @Input() playerStats: PlayerStats;
  @Input() loading: boolean;
  constructor() {}
}
