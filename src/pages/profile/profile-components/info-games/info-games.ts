import { Component, Input } from '@angular/core';
import { NavController } from 'ionic-angular/navigation/nav-controller';

@Component({
  selector: 'info-games',
  templateUrl: './info-games.html'
})
export class InfoGamesComponent {
  @Input() playedGames: any;
  @Input() loading: boolean;
  constructor(private navCtrl: NavController) {}

  openGameStats(gameId: string) {
    this.navCtrl.push('GameStatsPage', { gameId: gameId });
  }

  listAllGames() {
    this.navCtrl.setRoot('GamesListPage', { playedGames: this.playedGames }, { animate: true, direction: 'forward' });
  }
}
