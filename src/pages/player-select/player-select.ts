import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { PlayerProfile } from '../../models/index';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';
/**
 * Generated class for the PlayerSelectPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-player-select',
  templateUrl: 'player-select.html'
})
export class PlayerSelectPage {
  private tag: string;
  private excludePlayers: string[];

  userCollection: AngularFirestoreCollection<PlayerProfile>;
  users: Observable<PlayerProfile[]>;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private view: ViewController,
    private firestore: AngularFirestore
  ) {
    this.tag = this.navParams.get('tag');
    this.excludePlayers = this.navParams.get('excludePlayers');
    console.log(this.excludePlayers);

    this.userCollection = firestore.collection('playerProfile');
    this.users = this.userCollection.valueChanges().map(users => users.filter(user => this.excludePlayers.indexOf(user.id) === -1));
  }

  selectPlayer(playerProfile) {
    this.view.dismiss(playerProfile);
  }

  dismissModal() {
    this.view.dismiss();
  }
}
