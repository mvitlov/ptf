import { Component, OnInit, OnDestroy } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, LoadingController, AlertController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms/';
import { AuthenticationProvider } from '../../providers/authentication/authentication';
import { ProfileProvider } from '../../providers/profile/profile';

/**
 * Generated class for the SigninPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-signin',
  templateUrl: 'signin.html'
})
export class SigninPage {
  public signinForm: FormGroup;

  constructor(
    private alertCtrl: AlertController,
    private loadingCtrl: LoadingController,
    public navCtrl: NavController,
    private menu: MenuController,
    public navParams: NavParams,
    private formBuilder: FormBuilder,
    private auth: AuthenticationProvider,
    private profile: ProfileProvider
  ) {
    this.signinForm = this.formBuilder.group(
      {
        email: ['', Validators.compose([Validators.required, Validators.email])],
        password: ['', Validators.compose([Validators.minLength(6), Validators.required])]
      },
      { validator: [this.matchingDomain('email')] }
    );
  }
  ionViewDidEnter() {
    this.menu.enable(false);
  }

  private matchingDomain(emailKey: string): { [key: string]: any } {
    return (group: FormGroup): { [key: string]: any } => {
      const email: string = group.controls[emailKey].value;

      if (email.indexOf('@pis.eu.com') === -1) {
        return { invalidDomain: true };
      }
    };
  }

  public async onSigninUser(): Promise<void> {
    const loading = await this.loadingCtrl.create();
    try {
      loading.present();
      const userCredentials = await this.auth.signInUser(this.signinForm.value.email, this.signinForm.value.password);

      this.profile.profileExists(userCredentials.user.uid).then(profile => {
        if (profile) {
          this.navCtrl.setRoot('ProfilePage');
          loading.dismiss();
        } else {
          this.navCtrl.setRoot('PlayerNewPage');
          loading.dismiss();
        }
      });
    } catch (error) {
      await loading.dismiss();
      const alert = await this.alertCtrl.create({
        message: error.message,
        buttons: [
          {
            text: 'OK',
            role: 'cancel'
          }
        ]
      });
      await alert.present();
    }
  }

  public onCreateAccount(): void {
    this.navCtrl.push('SignupPage');
  }

  public onForgotPassword(): void {
    this.navCtrl.push('PasswordResetPage');
  }

  ionViewWillLeave() {
    this.menu.enable(true);
  }
}
