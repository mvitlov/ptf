import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ProfileProvider } from '../../providers/profile/profile';
import { AuthenticationProvider } from '../../providers/authentication/authentication';
import { GameProvider } from '../../providers/game/game';

/**
 * Generated class for the GamesListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-games-list',
  templateUrl: 'games-list.html'
})
export class GamesListPage {
  public playedGames: any[] = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private auth: AuthenticationProvider,
    private profile: ProfileProvider,
    private game: GameProvider
  ) {
    this.auth.getUserId().subscribe(userId => {
      this.game.getPlayerGames(userId).subscribe(games => (this.playedGames = this.profile.getPlayedGames(games, userId)));
    });
  }

  public openGameStats(gameId: string): void {
    this.navCtrl.push('GameStatsPage', { gameId: gameId });
  }
}
