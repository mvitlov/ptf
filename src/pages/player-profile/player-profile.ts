import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PlayerProfile, Game, PlayerStats } from '../../models/index';
import { GameProvider } from '../../providers/game/game';
import { ProfileProvider } from '../../providers/profile/profile';
import { OnInit } from '@angular/core/src/metadata/lifecycle_hooks';

/**
 * Generated class for the PlayerProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-player-profile',
  templateUrl: 'player-profile.html'
})
export class PlayerProfilePage implements OnInit {
  player: PlayerProfile = this.navParams.get('player');
  playerStats: PlayerStats;

  constructor(public navCtrl: NavController, public navParams: NavParams, private games: GameProvider, private profile: ProfileProvider) {}

  ngOnInit() {
    this.games.getPlayerGames(this.player.id).subscribe((playerGames: Game[]) => {
      this.playerStats = this.profile.getStats(playerGames, this.player.id);
    });
  }
  public openGameStats(gameId: string): void {
    this.navCtrl.push('GameStatsPage', { gameId: gameId });
  }
}
