import { Component } from '@angular/core';
import { IonicPage, NavController, AlertController, LoadingController } from 'ionic-angular';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { AuthenticationProvider } from '../../providers/authentication/authentication';

/**
 * Generated class for the PasswordResetPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-password-reset',
  templateUrl: 'password-reset.html'
})
export class PasswordResetPage {
  public resetPasswordForm: FormGroup;

  constructor(
    private auth: AuthenticationProvider,
    private alertCtrl: AlertController,
    private loadingCtrl: LoadingController,
    private formBuilder: FormBuilder,
    private navCtrl: NavController
  ) {
    this.resetPasswordForm = this.formBuilder.group(
      {
        email: ['', Validators.compose([Validators.required, Validators.email])]
      }
      // { validator: [this.matchingDomain('email')] }
    );
  }

  private matchingDomain(emailKey: string): { [key: string]: any } {
    return (group: FormGroup): { [key: string]: any } => {
      const email: string = group.controls[emailKey].value;
      if (email.indexOf('@pis.eu.com') === -1) {
        return { invalidDomain: true };
      }
    };
  }

  public async onResetPassword(resetPasswordForm: FormGroup): Promise<void> {
    const email: string = resetPasswordForm.value.email;
    const loading = await this.loadingCtrl.create();
    try {
      loading.present();
      await this.auth.resetPassword(email);
      await loading.dismiss();

      const alert = await this.alertCtrl.create({
        message: 'Provjerite svoj email za upute!',
        buttons: [
          {
            text: 'OK',
            role: 'cancel',
            handler: () => {
              this.navCtrl.pop();
            }
          }
        ]
      });
      await alert.present();
    } catch (error) {
      console.log(error);
      const message = this.parseError(error.code);

      await loading.dismiss();

      const alert = await this.alertCtrl.create({
        message: message,
        buttons: [
          {
            text: 'OK',
            role: 'cancel'
          }
        ]
      });
      await alert.present();
    }
  }

  private parseError(code: string): string {
    switch (code) {
      case 'auth/user-not-found': {
        return 'Korisnik s navedenom email adresom nije pronadjen!';
      }
      default: {
        return code;
      }
    }
  }
}
