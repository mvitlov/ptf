import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, Alert, ToastController, Toast } from 'ionic-angular';
import { AuthenticationProvider } from '../../providers/authentication/authentication';
import { ProfileProvider } from '../../providers/profile/profile';
import { PlayerProfile } from '../../models';
import { Observable } from 'rxjs';

/**
 * Generated class for the ProfileEditPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile-edit',
  templateUrl: 'profile-edit.html'
})
export class ProfileEditPage /* implements OnInit */ {
  public playerProfile: Observable<PlayerProfile> = this.profile.playerProfile.valueChanges();
  private playerId: string;
  data: PlayerProfile;
  constructor(
    private navCtrl: NavController,
    private navParams: NavParams,
    private alertCtrl: AlertController,
    private toastCtrl: ToastController,
    private auth: AuthenticationProvider,
    private profile: ProfileProvider
  ) {}

  ngOnInit() {
    this.getProfile();
  }

  public getProfile(): void {
    this.playerProfile.subscribe(data => (this.data = data));
  }

  public onUpdateName(): void {
    const alert: Alert = this.alertCtrl.create({
      message: 'Ime i prezime',
      inputs: [
        {
          name: 'fullName',
          placeholder: this.data.fullName
        }
      ],
      buttons: [
        { text: 'Odustani' },
        {
          text: 'Spremi',
          handler: data => {
            if (!data.fullName) {
              return;
            } else {
              this.profile.updateName(data.fullName).then(() => this.toastMessage('Ime i prezime spremljeno!'));
            }
          }
        }
      ]
    });

    alert.present();
  }

  public onUpdateStatus(): void {
    const alert: Alert = this.alertCtrl.create({
      message: 'Status',
      inputs: [
        {
          name: 'status',
          placeholder: this.data.status
        }
      ],
      buttons: [
        { text: 'Odustani' },
        {
          text: 'Spremi',
          handler: data => {
            if (!data.status) {
              return;
            } else {
              this.profile.updateName(data.status).then(() => this.toastMessage('Status spremljen!'));
            }
          }
        }
      ]
    });

    alert.present();
  }

  public onUpdateHandeness(): void {
    const alert: Alert = this.alertCtrl.create({
      message: 'Dominantna ruka',
      inputs: [
        {
          type: 'radio',
          label: 'Desna',
          value: 'Desna',
          checked: this.data.handeness === 'Desna'
        },
        {
          type: 'radio',
          label: 'Lijeva',
          value: 'Lijeva',
          checked: this.data.handeness === 'Lijeva'
        }
      ],
      buttons: [
        { text: 'Odustani' },
        {
          text: 'Spremi',
          handler: data => {
            if (!data) {
              return;
            } else {
              this.profile.updateHandeness(data).then(() => this.toastMessage('Domin. ruka spremljena!'));
            }
          }
        }
      ]
    });

    alert.present();
  }

  public onUpdatePosition(): void {
    const alert: Alert = this.alertCtrl.create({
      message: 'Pozicija',
      inputs: [
        {
          type: 'radio',
          label: 'Napad',
          value: 'Napad',
          checked: this.data.position === 'Napad'
        },
        {
          type: 'radio',
          label: 'Obrana',
          value: 'Obrana',
          checked: this.data.position === 'Obrana'
        }
      ],
      buttons: [
        { text: 'Odustani' },
        {
          text: 'Spremi',
          handler: data => {
            if (!data) {
              return;
            } else {
              this.profile.updatePosition(data).then(() => this.toastMessage('Pozicija spremljena!'));
            }
          }
        }
      ]
    });

    alert.present();
  }

  public onUpdateEmail() {
    let alert: Alert = this.alertCtrl.create({
      message: 'Promjena email adrese',
      inputs: [{ name: 'newEmail', placeholder: 'Nova email adresa' }, { name: 'password', placeholder: 'Lozinka', type: 'password' }],
      buttons: [
        { text: 'Odustani' },
        {
          text: 'Spremi',
          handler: data => {
            this.profile
              .updateEmail(data.newEmail, data.password)
              .then(() => {
                this.toastMessage('Email adresa spremljena!');
              })
              .catch(error => {
                console.log(error);

                this.toastMessage('Greška! Pokušajte ponovno.');
              });
          }
        }
      ]
    });
    alert.present();
  }

  public onUpdatePassword() {
    let alert: Alert = this.alertCtrl.create({
      message: 'Promjena lozinke',
      inputs: [{ name: 'newPassword', placeholder: 'Nova lozinka', type: 'password' }, { name: 'oldPassword', placeholder: 'Stara lozinka', type: 'password' }],
      buttons: [
        { text: 'Odustani' },
        {
          text: 'Spremi',
          handler: data => {
            this.profile
              .updatePassword(data.newPassword, data.oldPassword)
              .then(() => {
                this.toastMessage('Lozinka spremljena!');
              })
              .catch(error => {
                console.log(error);

                this.toastMessage('Greška! Pokušajte ponovno.');
              });
          }
        }
      ]
    });
    alert.present();
  }

  private toastMessage(message: string) {
    let toast: Toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'bottom',
      showCloseButton: true,
      closeButtonText: 'OK'
    });
    toast.present();
  }
}
