import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { GamePlayers, PlayerProfile } from '../../models';
import { ProfileProvider } from '../../providers/profile/profile';
import { AuthenticationProvider } from '../../providers/authentication/authentication';

/**
 * Generated class for the GameNewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-game-new',
  templateUrl: 'game-new.html'
})
export class GameNewPage {
  public gamePlayers: GamePlayers = {};
  public fixedPlayer: string;
  public goalRule = false;
  public isGameReady = false;

  constructor(public navCtrl: NavController, private profile: ProfileProvider, private auth: AuthenticationProvider) {
    this.getProfile();
  }

  private getProfile(): void {
    this.auth.getUserId().subscribe(userId => {
      this.fixedPlayer = userId;
      this.profile.getPlayerProfile(userId).subscribe(profile => (this.gamePlayers.blue1 = profile));
    });
  }

  public selectPlayer(data: { player: PlayerProfile; tag: string }) {
    this.gamePlayers[data.tag] = data.player;
    this.checkIfGameReady();
  }

  public removePlayer(tag: string): void {
    delete this.gamePlayers[tag];
    this.checkIfGameReady();
  }

  public swapPositions(side: string): void {
    switch (side) {
      case 'blue': {
        [this.gamePlayers.blue1, this.gamePlayers.blue2] = [this.gamePlayers.blue2, this.gamePlayers.blue1];
        break;
      }
      case 'red': {
        [this.gamePlayers.red1, this.gamePlayers.red2] = [this.gamePlayers.red2, this.gamePlayers.red1];
        break;
      }
    }
  }

  public onSwapTeams(): void {
    [this.gamePlayers.blue1, this.gamePlayers.red1] = [this.gamePlayers.red1, this.gamePlayers.blue1];
    [this.gamePlayers.blue2, this.gamePlayers.red2] = [this.gamePlayers.red2, this.gamePlayers.blue2];
  }

  public allSelected(): boolean {
    return Object.keys(this.gamePlayers).length === 4;
  }

  private checkIfGameReady(): void {
    if (Object.keys(this.gamePlayers).map(key => this.gamePlayers[key].id).length === 4) {
      this.isGameReady = true;
    } else {
      this.isGameReady = false;
    }
  }

  public startGame(): void {
    this.navCtrl.setRoot('GamePlayPage', { gamePlayers: this.gamePlayers, goalRule: this.goalRule });
  }
}
