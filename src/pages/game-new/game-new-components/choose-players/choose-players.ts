import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { GamePlayers, Side, PlayerProfile } from '../../../../models';
import { ModalController } from 'ionic-angular';
import { PlayerSelectPage } from '../../../player-select/player-select';

@Component({
  selector: 'choose-players',
  templateUrl: 'choose-players.html'
})
export class ChoosePlayersComponent {
  @Input() side: Side;
  @Input() gamePlayers: GamePlayers = {};
  @Input() fixedPlayer: string;
  @Output() onPlayerSelect = new EventEmitter<{ player: PlayerProfile; tag: string }>();
  @Output() onPlayerRemove = new EventEmitter<string>();
  @Output() onPositionSwap = new EventEmitter<string>();

  constructor(private modalCtrl: ModalController) {}

  public isCurrentUser(tag: string): boolean {
    if (this.gamePlayers[tag]) {
      if (this.gamePlayers[tag].id === this.fixedPlayer) {
        return true;
      } else {
        return false;
      }
    } else {
      // don't show button if no player position selected
      return true;
    }
  }

  public async onSelectPlayer(tag: string): Promise<void> {
    if (this.gamePlayers[tag] && this.gamePlayers[tag].id === this.fixedPlayer) {
      return;
    }

    const modal = await this.modalCtrl.create('PlayerSelectPage', {
      tag: tag,
      excludePlayers: this.excludePlayers()
    });

    await modal.present();

    modal.onDidDismiss(playerProfile => {
      if (playerProfile) {
        this.onPlayerSelect.emit({ player: playerProfile, tag: tag });
      }
    });
  }

  public onRemovePlayer(tag: string): void {
    this.onPlayerRemove.emit(tag);
  }

  public onSwapPositions(side: string) {
    this.onPositionSwap.emit(side);
  }

  // RETURNS ARRAY OF ALREADY SELECED PLAYER ID'S TO EXCLUDE FROM LIST OF AVAILABLE PLAYERS
  private excludePlayers(): string[] {
    return Object.keys(this.gamePlayers).map(tag => this.gamePlayers[tag].id);
  }
}
