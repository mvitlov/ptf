import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GameNewPage } from './game-new';
import { ChoosePlayersComponent } from './game-new-components/choose-players/choose-players';

@NgModule({
  declarations: [GameNewPage, ChoosePlayersComponent],
  imports: [IonicPageModule.forChild(GameNewPage)]
})
export class GameNewPageModule {}
